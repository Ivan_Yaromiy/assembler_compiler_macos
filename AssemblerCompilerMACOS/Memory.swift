//
//  Memory.swift
//  AssemblerCompilerMACOS
//
//  Created by Admin on 15.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Cocoa

class Memory: NSObject {
    
    let MIN_VALUE = 0
    let MAX_VALUE = 255
    
    let MIN_ADRESS = 0
    let MAX_ADRESS = 65535
    
    subscript(i: Int) -> Int {
        get {
            return Model.arrayMemory[i]
        }
        set(value){
            if ((value >= MIN_VALUE) && (value <= MAX_VALUE)) {
                Model.arrayMemory[i] = value
            } else {
                print("Memory is overloaded")
            }
        }
    }
}
