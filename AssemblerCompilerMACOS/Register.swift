//
//  Register.swift
//  AssemblerCompilerMACOS
//
//  Created by Admin on 14.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Cocoa

class Register: NSObject {
  
    let MIN_VALUE = 0
    let MAX_VALUE = 255
    
    let MIN_ADRESS = 0
    let MAX_ADRESS = 15

    subscript(index: Int) -> Int {
        get {
            return Model.arrayRegister[index]
        }
        set(value){
            if ((value >= MIN_VALUE) && (value <= MAX_VALUE)) {
                Model.arrayRegister[index] = value
            } else {
                print("Register is overloaded")
            }
        }
    }
}
