//
//  Register.swift
//  AssemblerCompilerMACOS
//
//  Created by Admin on 14.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Cocoa

class WorkViewController: NSViewController {
    
    //var modelController = ModelController()
    var processor = Processor()
    var scanner = Scanner()
    var register = Register()
    
    @IBOutlet weak var register0: NSTextField!
    @IBOutlet weak var register1: NSTextField!
    @IBOutlet weak var register2: NSTextField!
    @IBOutlet weak var register3: NSTextField!
    @IBOutlet weak var register4: NSTextField!
    @IBOutlet weak var register5: NSTextField!
    @IBOutlet weak var register6: NSTextField!
    @IBOutlet weak var register7: NSTextField!
    @IBOutlet weak var register8: NSTextField!
    @IBOutlet weak var register9: NSTextField!
    @IBOutlet weak var register10: NSTextField!
    @IBOutlet weak var register11: NSTextField!
    @IBOutlet weak var register12: NSTextField!
    @IBOutlet weak var register13: NSTextField!
    @IBOutlet weak var register14: NSTextField!
    @IBOutlet weak var register15: NSTextField!
    
    func registerView() {
        register0.stringValue = String(Model.arrayRegister[0])
        register1.stringValue = String(Model.arrayRegister[1])
        register2.stringValue = String(Model.arrayRegister[2])
        register3.stringValue = String(Model.arrayRegister[3])
        register4.stringValue = String(Model.arrayRegister[4])
        register5.stringValue = String(Model.arrayRegister[5])
        register6.stringValue = String(Model.arrayRegister[6])
        register7.stringValue = String(Model.arrayRegister[7])
        register8.stringValue = String(Model.arrayRegister[8])
        register9.stringValue = String(Model.arrayRegister[9])
        register10.stringValue = String(Model.arrayRegister[10])
        register11.stringValue = String(Model.arrayRegister[11])
        register12.stringValue = String(Model.arrayRegister[12])
        register13.stringValue = String(Model.arrayRegister[13])
        register14.stringValue = String(Model.arrayRegister[14])
        register15.stringValue = String(Model.arrayRegister[15])
    }
    
    @IBOutlet var textView: NSTextView!
    
    @IBOutlet weak var ttt: NSButton!
    
    @IBAction func ppp(_ sender: Any) {
        textView.string = Model.fileASM.text
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        textView.string = Model.fileASM.text
    }
    
    func readNext() -> String {
        let data: String = Model.fileArray.text.remove(at: 0)
        return data
    }
    
    func getRegisterNumber(register: String) -> Int {
        if ((register.starts(with: "R")) || (register.starts(with: "r"))){
            print("\(register)")
            return Int(register.dropFirst())!
        }
        return -1
    }
    
    @IBAction func performComandButton(_ sender: Any) {
        print("\(Model.fileArray.text)")
        let statement: String = readNext()
        var reg1: String
        var reg2: String
        var addr1: Int
        var addr2: Int
        var value: Int
        var mem: Int
        
        switch (statement) {
        case "li":
            reg1 = readNext()
            addr1 = getRegisterNumber(register: reg1)
            if (addr1 != -1) {
                value = Int(readNext())!
                processor.li(address: addr1, value: value)
            }
            break
        case "or":
            reg1 = readNext()
            reg2 = readNext()
            addr1 = getRegisterNumber(register: reg1)
            addr2 = getRegisterNumber(register: reg2)
            if (addr1 != -1) && (addr2 != -1) {
                processor.or(addr1: addr1, addr2: addr2)
            }
            break
        case "and":
            reg1 = readNext()
            reg2 = readNext()
            addr1 = getRegisterNumber(register: reg1)
            addr2 = getRegisterNumber(register: reg2)
            if (addr1 != -1) && (addr2 != -1) {
                processor.and(addr1: addr1, addr2: addr2)
            }
            break
        case "add":
            reg1 = readNext()
            reg2 = readNext()
            addr1 = getRegisterNumber(register: reg1)
            addr2 = getRegisterNumber(register: reg2)
            if (addr1 != -1) && (addr2 != -1) {
                processor.add(addr1: addr1, addr2: addr2)
            }
            break
        case "sub":
            reg1 = readNext()
            reg2 = readNext()
            addr1 = getRegisterNumber(register: reg1)
            addr2 = getRegisterNumber(register: reg2)
            if (addr1 != -1) && (addr2 != -1) {
                processor.sub(addr1: addr1, addr2: addr2)
            }
            break
        case "sw":
            reg1 = readNext()
            addr1 = getRegisterNumber(register: reg1)
            mem = Int(readNext())!
            if (mem > 0) {
                processor.sw(registerAddress: addr1, memoryAddress: mem)
            }
            break
        case "lw":
            reg1 = readNext()
            addr1 = getRegisterNumber(register: reg1)
            mem = Int(readNext())!
            if (mem > 0) {
                processor.lw(registerAddress: addr1, memoryAddress: mem)
            }
            break
        default:
            print("No command")
        }
        registerView()
        
    }
}
