//
//  ViewController.swift
//  AssemblerCompilerMACOS
//
//  Created by Admin on 13.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    
    var string1: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBOutlet weak var filenameField: NSTextField!
    
    @IBAction func browseFile(_ sender: Any) {
        let dialog = NSOpenPanel();
        dialog.title                   = "Choose a .asm file";
        dialog.showsResizeIndicator    = true;
        dialog.showsHiddenFiles        = false;
        dialog.canChooseDirectories    = true;
        dialog.canCreateDirectories    = true;
        dialog.allowsMultipleSelection = false;
        dialog.allowedFileTypes        = ["asm"];
        
        if (dialog.runModal() == NSApplication.ModalResponse.OK) {
            let result = dialog.url // Pathname of the file
            if (result != nil) {
                let path = result!.path
                filenameField.stringValue = path
                string1 = path
            }
        } else {
            // User clicked on "Cancel"
            return
        }
    }
    
    @IBAction func oK(_ sender: Any) {
        let location = NSString(string: string1).expandingTildeInPath
        let fileContent = try? NSString(contentsOfFile: location, encoding: String.Encoding.utf8.rawValue)
        Model.fileASM = StringFile(text: fileContent! as String)
        let separated = Model.fileASM.text.components(separatedBy: ["\n", "\t", " "])
        Model.fileArray = ArrayFile(text: separated)
        
        dismiss(self)
    }
}
