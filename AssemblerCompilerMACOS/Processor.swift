//
//  Processor.swift
//  AssemblerCompilerMACOS
//
//  Created by Admin on 15.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

class Processor: NSObject {
    
    var registerClass = Register()
    var memoryClass = Memory()
    var pc = 0
    
    func nop() -> Int {
        pc+=1
        return pc
    }
    
    func checkAddress(registerAddress: Int, memoryAddress: Int) -> Bool {
        if((registerAddress >= registerClass.MIN_ADRESS) && (registerAddress <= registerClass.MAX_ADRESS) && (memoryAddress >= memoryClass.MIN_VALUE) && (memoryAddress <= memoryClass.MAX_VALUE)) {
            return true
        } else {return false}
    }
    
    func sw(registerAddress: Int, memoryAddress: Int) {
        if(checkAddress(registerAddress: registerAddress, memoryAddress: memoryAddress)) {
            memoryClass[memoryAddress] = registerClass[registerAddress]
        }
    }
    
    func lw(registerAddress: Int, memoryAddress: Int) {
        if(checkAddress(registerAddress: registerAddress, memoryAddress: memoryAddress)) {
            registerClass[registerAddress] = memoryClass[memoryAddress]
        }
    }
    
    func checkRange(addr1: Int, addr2: Int) -> Bool {
        if((addr1 >= registerClass.MIN_ADRESS) && (addr1 <= registerClass.MAX_ADRESS) && (addr2 >= registerClass.MIN_VALUE) && (addr2 <= registerClass.MAX_VALUE)) {
            return true
        }
        return false
    }
    
    func li(address: Int, value: Int) {
        if((address >= registerClass.MIN_ADRESS) && (address <= registerClass.MAX_ADRESS)) {
            registerClass[address] = value
        }
    }
    
    func add(addr1: Int, addr2: Int) {
        if(checkRange(addr1: addr1, addr2: addr2)) {
            registerClass[addr1] = registerClass[addr1] + registerClass[addr2]
        } else {
            print("There is no such address")
        }
    }
    
    func sub(addr1: Int, addr2: Int) {
        if(checkRange(addr1: addr1, addr2: addr2)) {
            registerClass[addr1] = registerClass[addr1] - registerClass[addr2]
        } else {
            print("There is no such address")
        }
    }
    
    func and(addr1: Int, addr2: Int) {
        if(checkRange(addr1: addr1, addr2: addr2)) {
            registerClass[addr1] = registerClass[addr1] & registerClass[addr2]
        } else {
            print("There is no such address")
        }
    }
    
    func or(addr1: Int, addr2: Int) {
        if(checkRange(addr1: addr1, addr2: addr2)) {
            registerClass[addr1] = registerClass[addr1] | registerClass[addr2]
        } else {
            print("There is no such address")
        }
    }
}
