//
//  Scanner.swift
//  AssemblerCompilerMACOS
//
//  Created by Admin on 16.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Cocoa

class Scanner: NSViewController {
    //var ttr:ArrayFile = ArrayFile()
    var modelController = ModelController()
    
    func readNext() -> String {
        print("\(modelController.fileArray.text)")
        let data: String = modelController.fileArray.text.remove(at: 0)
        return data
    }
    
}
