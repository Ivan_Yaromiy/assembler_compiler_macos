//
//  File.swift
//  AssemblerCompilerMACOS
//
//  Created by Admin on 15.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

struct StringFile {
    let text: String
}

struct ArrayFile {
    var text: [String]
}

