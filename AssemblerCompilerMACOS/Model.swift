//
//  Model.swift
//  AssemblerCompilerMACOS
//
//  Created by Admin on 10.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

class Model {
    
    static var fileASM = StringFile(
        text: "Not File"
    )
    
    static var fileArray = ArrayFile(
        text: []
    )
    
    static var arrayRegister:[Int] = Array(repeating: 0, count: 16)
    
    static var arrayMemory:[Int] = Array(repeating: 0, count: 65535)
    
}
